var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/agrocamp');

var userSchema = new mongoose.Schema({
    email: String,
    perfil: String,
    password: String
},
    { collection: 'users' }
);


var produtoSchema = new mongoose.Schema({
    nome: String,
    descricao: String,
    id_imagem: String,
    valor: Number,
    peso: Number
},
    { collection: 'produtos' }
);
var produtos = mongoose.model('produtos', produtoSchema, 'produtos');
var users = mongoose.model('users', userSchema, 'users');

module.exports = {
    Mongoose: mongoose,
    UserSchema: userSchema,
    Users: users,
    Produtos: produtos
}