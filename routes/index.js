const express = require('express');
const jwt = require('jsonwebtoken');
const db = require('../db');
var cfg = require("../config");

var router = express.Router();


/* GET home page. */
router.get('/', function (req, res, next) {
  //TODO: Capturar produtos em destque (ordenar pelo produto mais vendido)
  db.Produtos.find({}).lean().exec(
    function (e, docs) {
      res.render('index', { "produtoslist": docs });
    }
  );
});



router.get('/produto/frete/:cep', function (req, res) {

if(req.params.cep.length != 8){
  res.json({Mensagem:'CEP Invalido'});
  return;
}
  var request = require('request');
  request('http://correios-server.herokuapp.com/frete/prazo?nCdServico=41106&sCepOrigem=13045603&sCepDestino='+req.params.cep+'&nVlPeso=1&nCdFormato=1&nVlComprimento=20&nVlAltura=4&nVlLargura=11&nVlDiametro=20&nVlValorDeclarado=500', function (error, response, body) {
    if(response.statusCode==200)
    {
      
      var dados = JSON.parse(body).response[0];
      console.log(JSON.parse(body).response[0].Valor);
      res.json({Mensagem:'',Valor:dados.Valor, prazo:dados.PrazoEntrega});
    }
    else
      res.json({Mensagem:'Erro cálculo frete'});    
    return;
  });
  return;
});


router.get('/produto/:producId', function (req, res) {

  var idProduto = req.params.producId;
  console.log();

  if(!idProduto || idProduto.length!=24){
    res.redirect('/');
    return;
  }
  
    db.Produtos.findById(idProduto, function (error, produto) {

    if (error){
      console.log(error);
      throw error;
    }

    console.log(produto);
      if(produto==null){
        res.redirect('/');
        return;
      }
  
      
    console.log('produto:' + produto.nome);
    res.render("produto", { "produto": produto });

  });


});





/* Integrar com sistema legado */
router.get('/sendRabbitMQ', function (req, res) {
  db.Users.findOne({}, function (error, user) {
    if (error)
      throw error;

    if (user) {
      var amqp = require('amqplib/callback_api');

      amqp.connect('amqp://localhost', function (err, conn) {
        conn.createChannel(function (err, ch) {
          var q = 'hello';

          ch.assertQueue(q, { durable: true });
          // Note: on Node 6 Buffer.from(msg) should be used
          var now = new Date();
          var isoString = now.toISOString();
          var teste = ({ 'Message': 'Hello World!', 'Data': isoString });
          ch.sendToQueue(q, new Buffer(JSON.stringify(user)));
          ch.close();
          //conn.close();
          console.log(" [x] Sent 'Hello World!'");
          res.json({ message: 'integrando' });
        });
      });

    }
  });
});

/* Receive integração */
router.get('/receiveRabbitMQ', function (req, res) {

  var amqp = require('amqplib/callback_api');

  amqp.connect('amqp://localhost', function (err, conn) {
    conn.createChannel(function (err, ch) {
      var q = 'hello';
      ch.assertQueue(q, { durable: true });

      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
      ch.consume(q,

        function (msg) {

          //code before the pause
          console.log(" [x] Received %s", msg.content.toString());


        }, { noAck: true });

    });
  });

});




router.get('/api', (req, res) => {
  res.json({
    message: 'Welcome to the API'
  });
});

router.post('/api/post', verifyToken, (req, res) => {
  res.json({
    message: 'Post to the API'
  });
});

router.post('/api/login', (req, res) => {
  const user = {
    id: 1, username: 'tiago', email: 'ticarvalho@ymail.com'
  }

  jwt.sign({ user }, 'secretkey', (err, token) => {
    res.json({
      token
    });
  });
});


function verifyToken(req, res, next) {
  //get auth header value
  const bearerHeader = req.headers['authorization'];
  //check is undefined
  if (typeof bearerHeader !== undefined) {


  } else {
    res.sendStatus(403);
  }

}

/* GET new page. */
router.get('/new', function (req, res, next) {
  res.render('new', { title: 'Cadastro' });
});

/* POST new page. */
router.post('/new', function (req, res, next) {
  var nome = req.body.nome;
  res.redirect('/?nome=' + nome);
});


/* GET Userlist page. */
router.get('/login', function (req, res) {
  res.render('login');
});


router.post('/login', function (req, res) {
  //res.render('login',{message:'Usuário ou Senha inválidos!'});
  console.log(req.body);
  if (2 == 2) {
    db.Users.findOne({
      email: req.body.email,
      password: req.body.password
    }, function (error, user) {
      if (error)
        throw error;

      if (!user) {

        //Render view
        //res.render('login.ejs',{ auth: false, message:'Usuário ou Senha inválidos!' });
        if (1 == 2)
          res.status(200).send({ auth: false, message: 'Usuário ou Senha inválidos!' });
        if (1 == 2)
          res.json({
            sucess: false,
            message: 'Usuário ou senha inválidos!'
          });
      } else {

        // Criar JWT
        var token = jwt.sign({
          data: user.email && user.password,
          exp: Math.floor(Date.now() / 1000) + (60)
          //expiresInMinutes: 1440 //o token irá expirar em 24 horas
        }, cfg.superNodeAuth);

        //res.writeHead(statusCode, [reasonPhrase], [headers])

        //res.status(200).send({ auth: true, token: token });
        //retornar token
        res.json({
          success: true,
          message: 'token jwt',
          token: token
        });

      }
    });
  }
});


//TODO: Criar a rota middleware para poder verificar e autenticar o token
router.use(function (req, res, next) {

  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, cfg.superNodeAuth, function (err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Falha ao tentar autenticar o token!' });
      } else {
        //se tudo correr bem, salver a requisição para o uso em outras rotas
        req.decoded = decoded;
        next();
      }
    });

  } else {
    // se não tiver o token, retornar o erro 403
    return res.status(403).send({
      success: false,
      message: 'Não há token.'
    });
  }
});


/* POST adduser page. */
router.post('/adduser', function (req, res) {

  var email = req.body.email;
  var senha = req.body.password;
  var perfil = req.body.perfil;

  var user = new db.Users({ email: email, password: senha, perfil: perfil });
  user.save(function (err) {
    if (err) {
      console.log("Error! " + err.message);
    }
    else {
      console.log("Post saved!");
      res.redirect("userlist");
    }
  });
});


/* GET Userlist page. */
router.get('/userlist', function (req, res) {
  db.Users.find({}).lean().exec(
    function (e, docs) {
      res.render('userlist', { "userlist": docs });
    }
  );
});



router.get('/about', function (req, res) {

  var name = req.params.name;
  res.render('about', { title: name })

})

module.exports = router;
